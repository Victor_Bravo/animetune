# AnimeTune



## Descripción del Proyecto

AnimeTune es una aplicación de juego que muestra mis habilidades de desarrollo en Kotlin. El objetivo del juego es adivinar fragmentos de música de anime en diferentes modos de juego. Desafía tu conocimiento musical y disfruta de una experiencia interactiva.

## Requisitos

- Android 7.0 (API nivel 24) o superior
- 30 MB de almacenamiento libre

## Instalación

```
git clone https://gitlab.com/Victor_Bravo/animetune.git
cd AnimeTuneChallenge
```

## Uso

- inicia session con el usuario admin y contraseña admin

- eligir la modalidad de juego

## Licencia

AnimeTune está licenciado bajo la Boost Software License.

## Estatus del proyecto
El proyecto se encuentra actualmente en desarrollo. ¡Se esperan nuevas características y mejoras pronto!
