package com.example.animetunechallenge.ui.theme.screen.game.defeat

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController
import com.example.animetunechallenge.R
import com.example.animetunechallenge.navigation.Screen
import kotlinx.coroutines.delay

@Composable
fun Defeat(navController: NavHostController) {
    ConstraintLayout(modifier = Modifier.background(Color.Red).fillMaxSize()) {
        val (img, text) = createRefs()
        LaunchedEffect(Unit){
            delay(2000)
            navController.navigate(Screen.OptionScreen.route){
                popUpTo(Screen.OptionScreen.route){
                    inclusive = true
                }
            }
        }

        Text(
            modifier = Modifier
                .constrainAs(text) {
                    top.linkTo(parent.top)
                    end.linkTo(parent.end)
                    start.linkTo(parent.start)
                },

            text = "sigue intentando",
            style = TextStyle(
                fontSize = 45.sp,
                fontWeight = FontWeight(400),
                color = Color(0xFFFFFFFF),
                textAlign = TextAlign.Center,
            )
        )
        Image(
            modifier = Modifier.size(300.dp)
                .constrainAs(img) {
                    bottom.linkTo(parent.bottom)
                    end.linkTo(parent.end)
                },
            painter = painterResource(R.drawable.chica_triste),
            contentDescription = "image description",
        )
    }
}