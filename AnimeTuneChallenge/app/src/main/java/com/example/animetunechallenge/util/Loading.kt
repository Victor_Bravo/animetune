package com.example.animetunechallenge.util

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.example.animetunechallenge.R


@Preview
@Composable
fun loanding() {
    val preloaderLottieComposition by rememberLottieComposition(
        LottieCompositionSpec.RawRes(R.raw.cat)
    )

    val preloaderLottieComposition2 by rememberLottieComposition(
        LottieCompositionSpec.RawRes(R.raw.loading)
    )

    ConstraintLayout(modifier = Modifier.fillMaxSize().background(color = Color.White)) {
        val (cat, loading) = createRefs()

        LottieAnimation(
            composition = preloaderLottieComposition,
            contentScale = ContentScale.Crop,
            isPlaying = true,
            iterations = LottieConstants.IterateForever,
            modifier = Modifier.size(200.dp).constrainAs(cat) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
                bottom.linkTo(parent.bottom)
            },
        )

        LottieAnimation(
            composition = preloaderLottieComposition2,
            contentScale = ContentScale.FillWidth,
            isPlaying = true,
            iterations = LottieConstants.IterateForever,
            modifier = Modifier.padding(top = 30.dp).height(10.dp).width(400.dp).constrainAs(loading) {
                top.linkTo(cat.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            })
    }
}