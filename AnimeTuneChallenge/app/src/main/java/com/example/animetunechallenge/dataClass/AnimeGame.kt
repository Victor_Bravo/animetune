package com.example.animetunechallenge.dataClass

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import kotlinx.coroutines.flow.Flow


data class AnimeGame(
  val name:Anime? ,
  val question:String? = null,
  val isWinner:Boolean
)

