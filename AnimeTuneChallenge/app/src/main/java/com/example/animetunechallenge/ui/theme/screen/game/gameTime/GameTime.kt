package com.example.animetunechallenge.ui.theme.screen.game.gameTime

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.compose.LocalLifecycleOwner
import androidx.navigation.NavHostController
import com.example.animetunechallenge.R
import com.example.animetunechallenge.dataClass.GameOption
import com.example.animetunechallenge.dataClass.ProgressNumber
import com.example.animetunechallenge.dataClass.ResponseData
import com.example.animetunechallenge.navigation.Screen
import com.example.animetunechallenge.ui.theme.screen.game.gameFree.GameViewModel
import com.example.animetunechallenge.util.Disco
import com.example.animetunechallenge.util.LogicResult
import com.example.animetunechallenge.util.OptionButton
import com.example.animetunechallenge.util.ProgressBarWithTime
import com.example.animetunechallenge.util.Header
import com.example.animetunechallenge.util.loanding

@Composable
fun GameTime(
    navController: NavHostController,
    gameViewModel: GameViewModel = hiltViewModel()
) {
    var hasNavigated by remember { mutableStateOf(false) }
    val context = LocalContext.current
    val option by gameViewModel.option.collectAsState()
    val loadingValue by gameViewModel.loading.collectAsState()
    val lifecycle = LocalLifecycleOwner.current.lifecycle

    DisposableEffect(lifecycle) {
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_PAUSE) gameViewModel.pause()
            if (event == Lifecycle.Event.ON_RESUME) gameViewModel.start()
            if (event == Lifecycle.Event.ON_CREATE) {
                hasNavigated = false
                gameViewModel.initGame(context, GameOption.FREE)
            }
        }
        lifecycle.addObserver(observer)
        onDispose {
            gameViewModel.clear()
            lifecycle.removeObserver(observer)
        }
    }

    LogicResult(gameViewModel, navController, hasNavigated, Screen.GameTime.route) {
        hasNavigated = true
    }

    when (option) {
        is ResponseData.ErrorData -> {
            if (!hasNavigated) {
                navController.navigate(route = Screen.Bugs.route) {
                    hasNavigated = true
                    launchSingleTop = true
                }
            }
        }

        is ResponseData.SuccessData -> {
            ConstraintLayout(modifier = Modifier.fillMaxSize()) {
                val (boxHeader, boxImage, boxFooter, boxBody, progressBar) = createRefs()

                Header(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color(0xFF5147A6))
                        .padding(vertical = 5.dp, horizontal = 5.dp)
                        .constrainAs(boxHeader) {
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        },
                    level = option?.susses?.user?.level ?: 1,
                    text = "Tiempo"
                ) {
                    navController.popBackStack()
                }

                Disco(modifier = Modifier.constrainAs(boxImage) {
                    top.linkTo(boxHeader.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }, isMusicCompletion = gameViewModel.musicCompletion) {
                    if (!it || gameViewModel.musicCompletion.value) {
                        if (gameViewModel.musicCompletion.value) {
                            gameViewModel.restart(context)
                        } else gameViewModel.start()
                    } else gameViewModel.pause()
                }

                Box(modifier = Modifier.constrainAs(boxBody) {
                    top.linkTo(boxImage.bottom)
                }) {
                    OptionButton(Modifier, option?.susses?.anime, gameViewModel)
                }

                Image(painter = painterResource(id = R.drawable.girl_menu_option2),
                    contentDescription = "gird gamer option",
                    modifier = Modifier
                        .constrainAs(boxFooter) {
                            top.linkTo(boxBody.bottom)
                            bottom.linkTo(progressBar.top)
                            end.linkTo(parent.end)
                        }
                        .width(240.dp)
                        .height(260.dp),
                    contentScale = ContentScale.FillBounds
                )

                ProgressBarWithTime(
                    modifier = Modifier.constrainAs(progressBar){
                        bottom.linkTo(parent.bottom)
                    },
                    durationInSeconds = 12,
                    winner = gameViewModel.isWinner
                ) {
                    if (it == 0f) gameViewModel.defeat() else ProgressNumber.progressNumber =
                        it + 0.2f
                }
            }

        }

        null -> {}
    }
    if (loadingValue) loanding()

}
