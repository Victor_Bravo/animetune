package com.example.animetunechallenge.util

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.animetunechallenge.dataClass.ProgressNumber
import kotlinx.coroutines.flow.StateFlow

@Composable
fun ProgressBarWithTime(
    modifier: Modifier = Modifier,
    durationInSeconds: Int,
    winner: StateFlow<Boolean?>,
    onFinish: (Float) -> Unit,
) {
    val isWinner by winner.collectAsState()
    var progress by remember { mutableFloatStateOf(ProgressNumber.progressNumber) }
    val animatedProgress by animateFloatAsState(targetValue = progress,
        animationSpec = tween(durationMillis = durationInSeconds * 1000), label = ""
    )

    LaunchedEffect(key1 = Unit) {
        progress = 0f // Inicia la animación para llenar la barra
    }

    Column(modifier = modifier) {
        LinearProgressIndicator(
            color = when((animatedProgress * 100).toInt()){
                in 0..40 -> Color.Red
                in 41..80 -> Color.Yellow
                else -> Color.Green
            },
            progress = { animatedProgress },
            modifier = Modifier.fillMaxWidth().height(8.dp),
        )
//        Text("Progreso: ${(animatedProgress * 100).toInt()}%")
    }

    if (isWinner == true || (animatedProgress * 100).toInt() == 0) onFinish((animatedProgress))
}