package com.example.animetunechallenge.dataClass

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Entity(tableName = "anime_list")
data class AnimeMusic(
    @ColumnInfo(name = "name") val name: Anime,
    @ColumnInfo(name = "level") val level: Int,
    @ColumnInfo(name = "gameFree") val gameFree: Boolean = false,
    @ColumnInfo(name = "Enigma") val enigma: Boolean = false,
    @ColumnInfo(name = "Time") val time: Boolean = false,
    @ColumnInfo(name = "Question") val question: String,
    @PrimaryKey(autoGenerate = true) var id: Int = 0
)

@Dao
interface AnimeMusicDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(animeMusic: AnimeMusic)

    //update where name = name
    @Query("UPDATE anime_list SET gameFree = 1 WHERE name = :name")
    suspend fun updateGameWinner(name: Anime)

    @Query("SELECT * FROM anime_list where level = :level")
    fun getAll(level: Int): Flow<List<AnimeMusic>>

    @Query("DELETE FROM anime_list")
    suspend fun delete()
}

