package com.example.animetunechallenge.di

import android.content.Context
import androidx.room.Room
import com.example.animetunechallenge.dataClass.AnimeMusicDao
import com.example.animetunechallenge.dataSource.DbDataSource
import com.example.animetunechallenge.dataClass.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton



@Module
@InstallIn(SingletonComponent::class)//va a realilzar la inyeccion en toda la aplicacion
class DataSourceModule {
    @Singleton
    @Provides
    @Named("BaseUrl")
    fun providerBaseUrl() = "https://randomuser.me/api/"

    @Singleton
    @Provides
    fun dbDataSource(@ApplicationContext context: Context):DbDataSource{
        return Room.databaseBuilder(context, DbDataSource::class.java, "user_database")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun userDao(db: DbDataSource): UserDao = db.userDao()

    @Singleton
    @Provides
    fun animeMusicDao(db: DbDataSource): AnimeMusicDao = db.animeMusicDao()
}