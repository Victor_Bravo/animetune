package com.example.animetunechallenge.util

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.animetunechallenge.ui.theme.screen.game.gameFree.GameViewModel
import com.example.animetunechallenge.dataClass.AnimeGame

@Composable
fun ButtonGame(listAnime: AnimeGame? = null, gameViewModel: GameViewModel? = null) {
    var isWinner by remember { mutableStateOf<Boolean?>(null) }
    var listAnimeMutable by remember { mutableStateOf<AnimeGame?>(null) }
    DisposableEffect(listAnime){
        listAnimeMutable = listAnime
        onDispose {
            listAnimeMutable = listAnime
        }
    }
    Button(
        onClick = {
            if (gameViewModel?.isWinner?.value == null) {
                isWinner = listAnimeMutable?.isWinner
                if (listAnimeMutable?.isWinner == true) gameViewModel?.winner(listAnimeMutable) else gameViewModel?.defeat()
            }

        },
        colors = ButtonDefaults.buttonColors(
            containerColor = when (isWinner) {
                true -> Color.Green
                false -> Color.Red
                else -> Color.Transparent
            }
        ),
        shape = RoundedCornerShape(size = 25.dp),
        border = BorderStroke(width = 4.dp, color = Color(0xFF5147A6)),
        modifier = Modifier
            .padding(bottom = 14.dp)
            .width(166.dp).defaultMinSize(minHeight = 55.dp),
    ) {
        Text(
            text = listAnimeMutable?.name?.title ?: "",
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight(600),
                color = if (isWinner == false) Color.White else Color(0xFF272257),
                textAlign = TextAlign.Center,
            )
        )
    }
}