package com.example.animetunechallenge.dataClass

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Entity(tableName = "userData")
data class User(
    @ColumnInfo(name = "user") val user: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "lastName") val lastName: String,
    @ColumnInfo(name = "password") val password: String,
    @ColumnInfo(name = "level") val level: Int,
    @ColumnInfo(name = "session") val session: Boolean = false,
    @PrimaryKey(autoGenerate = true) var id: Int = 0
)

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User)

    @Query("SELECT * FROM userData ORDER BY id DESC")
    fun getAll(): Flow<List<User>>

    @Query("SELECT * FROM userData WHERE  session = 1")
    fun getUser(): Flow<List<User>>

    @Query("DELETE FROM userData")
    suspend fun delete()
}