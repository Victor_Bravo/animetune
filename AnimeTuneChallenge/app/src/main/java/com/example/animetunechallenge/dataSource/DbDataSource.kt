package com.example.animetunechallenge.dataSource

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.animetunechallenge.dataClass.AnimeMusic
import com.example.animetunechallenge.dataClass.AnimeMusicDao
import com.example.animetunechallenge.dataClass.User
import com.example.animetunechallenge.dataClass.UserDao

@Database(entities = [User::class, AnimeMusic::class] , version = 2)
abstract class DbDataSource : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun animeMusicDao(): AnimeMusicDao
}