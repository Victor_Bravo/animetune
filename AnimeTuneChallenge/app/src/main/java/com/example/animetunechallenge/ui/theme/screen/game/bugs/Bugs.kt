package com.example.animetunechallenge.ui.theme.screen.game.bugs

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.animetunechallenge.navigation.Screen


@Composable
fun Bugs(
    bugsViewModel: BugsViewModel = hiltViewModel(),
    navController: NavHostController
) {
    ConstraintLayout(modifier = Modifier.background(Color.Red).fillMaxSize()) {
        val (text, button) = createRefs()
        Text(
            modifier = Modifier.constrainAs(text){
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            },
            text = "upsss aun estamos programando"
        )
        Button(modifier = Modifier.constrainAs(button){
            top.linkTo(text.bottom)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
        }, onClick = {
            bugsViewModel.bug()
            navController.navigate(Screen.LoginScreen.route){
                popUpTo(Screen.OptionScreen.route){
                    inclusive = true
                }
            }
        }){
            Text(
                "reset bd"
            )
        }

    }
}