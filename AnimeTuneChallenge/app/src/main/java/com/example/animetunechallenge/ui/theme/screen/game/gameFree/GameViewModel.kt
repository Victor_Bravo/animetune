package com.example.animetunechallenge.ui.theme.screen.game.gameFree

import android.animation.ValueAnimator
import android.content.Context
import android.media.MediaPlayer
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.animetunechallenge.dataClass.AnimeGame
import com.example.animetunechallenge.dataClass.AnimeMusic
import com.example.animetunechallenge.dataClass.GameOption
import com.example.animetunechallenge.dataClass.Option
import com.example.animetunechallenge.dataClass.ResponseData
import com.example.animetunechallenge.repository.GameRepository
import com.example.animetunechallenge.util.musicRuta
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GameViewModel @Inject constructor(
    private val gameRepository: GameRepository
) : ViewModel() {
    var progress = 1f
    private var _isWinner = MutableStateFlow<Boolean?>(null)
    val isWinner: StateFlow<Boolean?> get() = _isWinner
    private var _option = MutableStateFlow<ResponseData<Option>?>(null)
    val option: StateFlow<ResponseData<Option>?> get() = _option
    private var _loading = MutableStateFlow(true)
    val loading: StateFlow<Boolean> get() = _loading
    private var _musicCompletion = MutableStateFlow(false)
    val musicCompletion: StateFlow<Boolean> get() = _musicCompletion
    private var victory: AnimeMusic? = null
    private var gameOptionGlob:GameOption? = null

    fun winner(listAnimeMutable: AnimeGame?) {
        _isWinner.value = true
        viewModelScope.launch {
            listAnimeMutable?.name?.let { gameRepository.saveAnimeWinner(it) }
        }
    }

    fun winnerReset() {
        _isWinner.value = null
    }

    fun clear() {
        instance = null
    }

    fun stop() {
        if (instance?.isPlaying == true) {
            instance?.stop()
            instance = null
        }
    }

    fun start() {
        instance?.start()
    }

    fun restart(context: Context) {
        initMusic(victory?.name?.name.orEmpty(), context = context)
    }

    fun pause() = instance?.pause()

    private fun initMusic(music: String, context: Context) {
        _musicCompletion.value = false
        musicRuta(music).createMediaPlayer(context)
        val fadeDuration = instance?.duration?.toLong() ?: 12000L
        val startVolume = 1.0f
        val endVolume = 0.0f
        val volumeAnimator = ValueAnimator.ofFloat(startVolume, endVolume)
        volumeAnimator.duration = fadeDuration
        volumeAnimator.addUpdateListener { animation ->
            val volume = animation.animatedValue as Float
            if (instance?.isPlaying == true) instance?.setVolume(volume, volume)
        }
        volumeAnimator.start()
        instance?.start()
        instance?.setOnCompletionListener {
            _musicCompletion.value = true
        }
    }

    fun initGame(context: Context, gameOption: GameOption) {
        viewModelScope.launch {
            try {
                _loading.value = true
                gameOptionGlob = gameOption
                val listAnime = gameRepository.loadingListAnime().shuffled()
                val user = gameRepository.loadingUser()
                val listOfPossibleWinners = listAnime.filter {
                    when(gameOption){
                        GameOption.FREE -> it.gameFree
                        GameOption.TIME -> it.time
                        else -> it.enigma
                    }
                     }
                victory = if (listOfPossibleWinners.isNotEmpty()) {
                    listAnime.minus(listOfPossibleWinners.toSet()).random()
                } else listAnime.random()
                val listWithoutVictory = listAnime - victory
                val randomElements = listWithoutVictory.shuffled().take(3)
                val resultList = (randomElements + victory).shuffled().map {
                    AnimeGame(name = it?.name, question = it?.question, isWinner = victory == it)
                }
                _option = MutableStateFlow(
                    ResponseData.SuccessData(Option(resultList.shuffled(), user))
                )
                delay(2000)

                _loading.value = false
                initMusic(victory?.name?.name.orEmpty(), context)

            } catch (e: Exception) {
                _option = MutableStateFlow(
                    ResponseData.ErrorData("$e")
                )
                Toast.makeText(context, "proximamente mas niveles", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        }
    }

    fun win() {
        viewModelScope.launch {
            val user = _option.value?.susses?.user
            gameRepository.updateUser(
                user?.copy(
                    level = user.level + 1,
                )
            )
        }
    }

    fun defeat() {
        _isWinner.value = false
        stop()
    }

    companion object {
        private var instance: MediaPlayer? = null

        fun Int.createMediaPlayer(context: Context) {
            // Libera la instance anterior si es play
            instance?.release()
            instance = MediaPlayer.create(context, this)
        }
    }

}