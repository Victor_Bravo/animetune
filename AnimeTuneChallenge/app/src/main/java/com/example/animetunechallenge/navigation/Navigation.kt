package com.example.animetunechallenge.navigation

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.animetunechallenge.ui.theme.screen.game.bugs.Bugs
import com.example.animetunechallenge.ui.theme.screen.game.defeat.Defeat
import com.example.animetunechallenge.ui.theme.screen.game.gameFree.GameFree
import com.example.animetunechallenge.ui.theme.screen.game.gameQuestion.GameQuestion
import com.example.animetunechallenge.ui.theme.screen.game.gameTime.GameTime
import com.example.animetunechallenge.ui.theme.screen.loginScreen.LoginScreen
import com.example.animetunechallenge.ui.theme.screen.optionScreen.OptionScreen
import com.example.animetunechallenge.ui.theme.screen.mainScreen.MainScreen
import com.example.animetunechallenge.ui.theme.screen.victory.VictoryAnimatedPreloader

@Composable
fun Navigation() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Screen.MainScreen.route) {

        composable(route = Screen.MainScreen.route) {
            MainScreen(navController = navController)
        }

        composable(route = Screen.OptionScreen.route) {
            OptionScreen(navController = navController)
        }

        composable(route = Screen.LoginScreen.route) {
            LoginScreen(navController = navController)
        }
        composable(route = Screen.Defeat.route) {
            Defeat(navController = navController)
        }
        composable(route = Screen.Bugs.route) {
            Bugs(navController = navController)
        }

        composable(route = Screen.GameQuestion.route){
            GameQuestion(navController = navController)
        }

        composable(route = Screen.GameFree.route, enterTransition ={
            slideIntoContainer(
                AnimatedContentTransitionScope.SlideDirection.Left,
                tween(1000)
            )
        }
        ) {
            GameFree(navController = navController)
        }

        composable(route = Screen.GameTime.route, enterTransition ={
            slideIntoContainer(
                AnimatedContentTransitionScope.SlideDirection.Left,
                tween(1000)
            )
        }){
            GameTime(navController = navController)
        }

        composable(route = Screen.Victory.route + "/{level}/{route}",
            arguments = listOf(
                navArgument("level") {
                    type = NavType.StringType
                    defaultValue = "1"
                    nullable = false
                },
                navArgument("route") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                }
            )
        ) {
            VictoryAnimatedPreloader(
                level = it.arguments?.getString("level") ?: "1",
                router = it.arguments?.getString("route") ?: "",
                navController = navController
            )

        }
    }
}
