package com.example.animetunechallenge.util

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.navigation.NavController
import com.example.animetunechallenge.navigation.Screen
import com.example.animetunechallenge.ui.theme.screen.game.gameFree.GameViewModel
import com.example.animetunechallenge.ui.theme.screen.victory.navigateData


@Composable
fun LogicResult(
    gameViewModel: GameViewModel,
    navController: NavController,
    hasNavigated: Boolean,
    router: String,
    onNavigate: () -> Unit
) {
    val winnerValue by gameViewModel.isWinner.collectAsState()
    winnerValue?.let {
        if (it) {
            gameViewModel.stop()
            gameViewModel.win()
            navController.navigate(
                Screen.Victory.withArgs(
                    gameViewModel.option.value?.susses?.user?.level.toString(),
                    router
                )
            ) {
                popUpTo(route = router){
                    inclusive = true
                }
                launchSingleTop = true

            }
            navigateData.navigate = false

        } else {
            gameViewModel.stop()
            if (!hasNavigated) {
                navController.navigate(Screen.Defeat.route) {
                    popUpTo(route = router){
                        inclusive = true
                    }
                    onNavigate()
                    launchSingleTop = true
                }
            }
        }
        gameViewModel.winnerReset()
    }
}