package com.example.animetunechallenge.util

import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.example.animetunechallenge.R

@Preview
@Composable
fun Header(modifier: Modifier = Modifier, level: Int = 0, text :String = "Libre", click: () -> Unit ={}) {

    ConstraintLayout(
        modifier = modifier
    ) {
        val (arrow, title, score) = createRefs()

        Image(
            modifier = Modifier.padding(1.dp)
                .width(30.dp)
                .height(45.dp).pointerInput(Unit) {
                    detectTapGestures {
                        click()
                    }
                }.constrainAs(arrow){
                    start.linkTo(parent.start)
                    top.linkTo(parent.top)
                },
            painter = painterResource(id = R.drawable.baseline_keyboard_backspace_24),
            contentDescription = "image description",
            colorFilter = ColorFilter.tint(Color.White)
        )
        Text(
            text,
            fontSize = 34.sp,
            color = Color.White,
            modifier = Modifier.constrainAs(title) {
                start.linkTo(arrow.end)
                top.linkTo(arrow.top)
                end.linkTo(score.start)
            }
        )
        Box(
            Modifier.constrainAs(score){
                end.linkTo(parent.end)
                top.linkTo(arrow.top)
            }, contentAlignment = Alignment.Center
        ) {
            Image(
                modifier = Modifier.padding(1.dp)
                    .width(50.dp)
                    .height(45.dp),
                painter = painterResource(id = R.drawable.baseline_grade_24),
                contentDescription = "image description",
                colorFilter = ColorFilter.tint(Color.White)
            )
            Text((level ?: 1).toString())
        }
    }
}
