package com.example.animetunechallenge.util

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun Question(question: String?) {
    Column  (modifier = Modifier.padding(40.dp)) {
        Text(
            text = "Este opening es de", textAlign = TextAlign.Center, style = TextStyle(
                fontSize = 34.sp,
                fontWeight = FontWeight(500),
                color = Color(0xFF5147A6),
            ), modifier = Modifier.fillMaxWidth()
                .alpha(0.7f)
                .padding(top = 20.dp)
        )
        Text(
            textAlign= TextAlign.Center,
            fontSize = 20.sp,
            modifier = Modifier
                .padding(top = 20.dp)
                .border(2.dp, color = Color(0xFF5147A6), shape = RoundedCornerShape(25.dp))
                .padding(10.dp),
            text = question.orEmpty()
        )

    }
}