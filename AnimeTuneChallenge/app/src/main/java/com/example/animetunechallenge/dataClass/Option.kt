package com.example.animetunechallenge.dataClass

data class Option(
    var anime: List<AnimeGame>,
    var user: User,
    )

sealed class ResponseData<T>(
    val susses : T?,
    val messageError: String?,

){
    class SuccessData<T>(success: T) : ResponseData<T>(success, null)
    class ErrorData<T>(messageError: String) : ResponseData<T>(null, messageError)
}