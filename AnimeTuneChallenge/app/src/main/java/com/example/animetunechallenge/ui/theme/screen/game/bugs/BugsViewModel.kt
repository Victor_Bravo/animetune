package com.example.animetunechallenge.ui.theme.screen.game.bugs

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.animetunechallenge.repository.GameRepository
import com.example.animetunechallenge.repository.LoginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BugsViewModel @Inject constructor(
    private val gameRepository: GameRepository
) : ViewModel() {

    fun bug(){
        viewModelScope.launch {
            gameRepository.delete()
        }
    }

}