package com.example.animetunechallenge.repository

import com.example.animetunechallenge.dataClass.Anime
import com.example.animetunechallenge.dataClass.AnimeMusic
import com.example.animetunechallenge.dataClass.AnimeMusicDao
import com.example.animetunechallenge.dataClass.User
import com.example.animetunechallenge.dataClass.UserDao
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class GameRepository @Inject constructor(
    private val user: UserDao,
    private val animeMusicDao: AnimeMusicDao
){

    suspend fun saveAnimeWinner(name:Anime){
        animeMusicDao.updateGameWinner(name = name)
    }

     suspend fun delete(){
        user.delete()
        animeMusicDao.delete()

    }

    suspend fun loadingListAnime():List<AnimeMusic>{
        return animeMusicDao.getAll(1).first()
    }

    suspend fun loadingUser(): User {
        return user.getUser().first()[0]
    }

    suspend fun updateUser(userData: User?){
        userData?.let {user.insert(it)  }
    }

}