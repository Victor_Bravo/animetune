package com.example.animetunechallenge.ui.theme.screen.loginScreen

import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.animetunechallenge.R
import com.example.animetunechallenge.navigation.Screen
import com.example.animetunechallenge.ui.theme.screen.mainScreen.BtnForm
import com.example.animetunechallenge.ui.theme.Purple80

@Composable
@Preview
fun LoginScreen(
    navController: NavHostController = NavHostController(LocalContext.current.applicationContext),
    loginViewModel: LoginViewModel = hiltViewModel()
) {
    val context = LocalContext.current.applicationContext
    val validationState by loginViewModel.validationState

    when (validationState) {
        is LoginViewModel.RespValidate.Error -> {
            Toast.makeText(
                context,
                (validationState as LoginViewModel.RespValidate.Error).message,
                Toast.LENGTH_SHORT
            ).show()
        }

        LoginViewModel.RespValidate.Success -> {
            navController.navigate(Screen.OptionScreen.route) {
                popUpTo(Screen.LoginScreen.route) { inclusive = true }
                launchSingleTop = true
            }
        }

        null -> {}
    }

    Surface(modifier = Modifier.background(Color.White)) {
        var isVisible by remember { mutableStateOf(false) }

        LaunchedEffect(Unit) {
            isVisible = true
        }

        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (topMax, bottomMax) = createRefs()
            Image(
                modifier = Modifier
                    .animateContentSize()
                    .fillMaxHeight()
                    .fillMaxWidth().constrainAs(topMax) {
                        top.linkTo(parent.top)
                    },
                contentScale = ContentScale.Crop,
                painter = painterResource(id = R.drawable.ambiente_fondo),
                contentDescription = "image description"
            )

            AnimatedVisibility(
                modifier = Modifier.fillMaxHeight(0.6f).constrainAs(bottomMax) {
                    bottom.linkTo(parent.bottom)
                },
                visible = isVisible,
                enter = slideInVertically(initialOffsetY = { fullHeight -> fullHeight }),
                exit = slideOutVertically(targetOffsetY = { fullHeight -> fullHeight })
            ) {

                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .fillMaxWidth()
                        .clip(RoundedCornerShape(topStart = 50.dp, topEnd = 50.dp))
                        .background(Color.White)
                        .padding(horizontal = 48.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,

                    ) {
                    Text(
                        text = "Welcome!", textAlign = TextAlign.Center, style = TextStyle(
                            fontSize = 30.sp,
                            fontWeight = FontWeight(500),
                            color = Color.Blue,
                        ), modifier = Modifier.fillMaxWidth()
                            .alpha(0.7f)
                            .padding(top = 20.dp)
                    )
                    var textUser by remember { mutableStateOf("") }
                    var textPassword by remember { mutableStateOf("") }
                    TextDecorate(
                        "Usuario",
                        modifier = Modifier.padding(top = 20.dp),
                        image = R.drawable.baseline_perm_identity_24
                    ) {
                        textUser = it
                    }
                    TextDecorate(
                        "Contraseña",
                        modifier = Modifier.padding(top = 20.dp),
                        isPassword = true,
                        image = R.drawable.lock
                    ) {
                        textPassword = it
                    }

                    Text(
                        "Registro",
                        modifier = Modifier.fillMaxWidth().padding(top = 8.dp, end = 8.dp),
                        textAlign = TextAlign.End,
                        style = TextStyle(
                            fontSize = 18.sp,
                            fontWeight = FontWeight(400),
                            color = Color.Blue,
                        )
                    )
                    BtnForm(text = "Log In", modifier = Modifier.padding(top = 20.dp), onClick = {
                        loginViewModel.validate(textUser, textPassword)
                    })
                }
            }
        }
    }
}

@Composable
fun TextDecorate(
    placeholder: String,
    modifier: Modifier = Modifier,
    image: Int? = null,
    isPassword: Boolean = false,
    onValueChange: (String) -> Unit
) {
    var passwordVisibility by remember { mutableStateOf(!isPassword) }
    var textState by remember { mutableStateOf(value = "") }
    val visualTransformation = if (passwordVisibility) {
        VisualTransformation.None
    } else PasswordVisualTransformation()

    TextField(
        modifier = modifier.fillMaxWidth().border(
            width = 2.dp,
            brush = Brush.horizontalGradient(listOf(Purple80, Purple80)),
            shape = RoundedCornerShape(35.dp)
        ),
        visualTransformation = visualTransformation,
        textStyle = TextStyle(
            fontSize = 20.sp
        ),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
        ),
        trailingIcon = {},
        leadingIcon = {
            if (image != null) {
                val icon = when {
                    isPassword -> if (!passwordVisibility) painterResource(image) else painterResource(
                        R.drawable.lock_open
                    )

                    else -> painterResource(image)
                }
                IconButton(onClick = { if (isPassword) passwordVisibility = !passwordVisibility }) {
                    Icon(painter = icon, contentDescription = "Icono descriptivo")
                }
            }
        },

        value = textState,
        onValueChange = {
            textState = it
            onValueChange(it)
        },
        placeholder = {
            Text(
                text = placeholder,
                modifier = Modifier.fillMaxWidth(),
                style = TextStyle(
                    color = Color.LightGray,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                ),
                textAlign = TextAlign.Center
            )
        },

        colors = TextFieldDefaults.colors(
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
            cursorColor = Color.Blue,
            focusedContainerColor = Color.Transparent,
            unfocusedContainerColor = Color.Transparent,
            disabledContainerColor = Color.Transparent,
        )
    )
}


