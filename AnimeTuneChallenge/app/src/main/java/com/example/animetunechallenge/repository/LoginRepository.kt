package com.example.animetunechallenge.repository

import com.example.animetunechallenge.dataClass.AnimeMusic
import com.example.animetunechallenge.dataClass.AnimeMusicDao
import com.example.animetunechallenge.dataClass.User
import com.example.animetunechallenge.dataClass.UserDao
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class LoginRepository @Inject constructor(
    private val userDao: UserDao,
    private val animeMusicDao: AnimeMusicDao
) {
   suspend fun listUser() = userDao.getAll().first()

   suspend fun createUser(user: User) = userDao.insert(user)
    suspend fun fillUser(user: User) {
        userDao.insert(user = user)
    }

    suspend fun fillListAnime(animeMusic: AnimeMusic){
        animeMusicDao.insert(animeMusic)

    }
}