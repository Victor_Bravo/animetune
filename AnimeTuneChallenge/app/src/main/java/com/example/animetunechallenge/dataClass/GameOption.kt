package com.example.animetunechallenge.dataClass

enum class GameOption {
    FREE,
    TIME,
    QUESTION,
}

object ProgressNumber {
    // Properties and methods of the singleton
    var progressNumber = 1f
}