package com.example.animetunechallenge.ui.theme.screen.victory

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController
import com.example.animetunechallenge.R
import com.example.animetunechallenge.navigation.Screen
import com.example.animetunechallenge.util.AnimatedPreloader
import com.example.animetunechallenge.util.Header

@Composable
fun VictoryAnimatedPreloader(
    level: String = "1",
    router: String,
    navController: NavHostController,
) {
    ConstraintLayout {
        val (img) = createRefs()
        Header(
            modifier = Modifier
                .fillMaxSize()
                .background(Color(0xFF5147A6))
                .padding(vertical = 5.dp, horizontal = 5.dp), level = level.toInt() + 1,
            text = "subiendo nivel"
        ) {
            if (!navigateData.navigate) {
                navController.navigate(Screen.OptionScreen.route)
                navigateData.navigate = true // Set the flag to true after navigating
            }
        }
        AnimatedPreloader(modifier = Modifier, level = level) { isFinish ->
            if (isFinish && !navigateData.navigate) {

                navController.navigate(router) {
                    launchSingleTop = true
                    popUpTo(Screen.Victory.withArgs(
                        level,
                        router
                    )) {
                        inclusive = true
                    }
                }
                navigateData.navigate = true // Set the flag to true after navigating
            }
        }

        Image(
            modifier = Modifier
                .size(320.dp)
                .constrainAs(img) {
                    bottom.linkTo(parent.bottom)
                    end.linkTo(parent.end)
                },
            painter = painterResource(R.drawable.victory),
            contentDescription = "image description",
        )
    }
}

object navigateData {
    var navigate: Boolean = false
}

