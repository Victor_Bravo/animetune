package com.example.animetunechallenge.util

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.animetunechallenge.dataClass.AnimeGame
import com.example.animetunechallenge.ui.theme.screen.game.gameFree.GameViewModel

@Composable
fun OptionButton(modifier: Modifier = Modifier, anime: List<AnimeGame>?, gameViewModel: GameViewModel) {
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .padding(start = 8.dp, end = 8.dp, bottom = 8.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = modifier.weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            ButtonGame(anime?.get(0), gameViewModel)
            ButtonGame(anime?.get(1), gameViewModel)
        }
        Column(
            modifier = Modifier.weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            ButtonGame(anime?.get(2), gameViewModel)
            ButtonGame(anime?.get(3), gameViewModel)
        }
    }
}