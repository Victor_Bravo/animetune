package com.example.animetunechallenge.ui.theme.screen.loginScreen

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.animetunechallenge.dataClass.Anime
import com.example.animetunechallenge.dataClass.AnimeMusic
import com.example.animetunechallenge.dataClass.User
import com.example.animetunechallenge.repository.LoginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val loginRepository: LoginRepository
) : ViewModel() {
    init {
        fillUser()
    }

    private val listMusicAnime = listOf(
        AnimeMusic(
            name = Anime.DRAGON_BALL,
            level = 1,
            question = "¿Cuál es el nombre del anime protagonizado por Goku y sus amigos mientras buscan las esferas del dragón?"
        ),
        AnimeMusic(
            name = Anime.NARUTO,
            level = 1,
            question = "¿Quién es el joven ninja con el sueño de convertirse en el Hokage de su aldea?"
        ),
        AnimeMusic(
            name = Anime.YUGIOH,
            level = 1,
            question = "¿En qué anime los personajes juegan un juego de cartas mágicas llamado “Duelo de Monstruos”?"
        ),
        AnimeMusic(
            name = Anime.DEATH_NOTE,
            level = 1,
            question = "¿Cuál es el título del anime donde un estudiante encuentra un cuaderno que le permite matar a cualquier persona cuyo nombre escriba en él?"
        ),
        AnimeMusic(
            name = Anime.ATTACK_ON_TITAN,
            level = 1,
            question = " ¿En qué anime la humanidad lucha contra gigantes devoradores de personas?"
        )
    )
    private val _validationState = mutableStateOf<RespValidate?>(null)
    val validationState: State<RespValidate?> = _validationState

    sealed class RespValidate {
        object Success : RespValidate()
        class Error(val message: String) : RespValidate()
    }

    private fun fillUser() {
        viewModelScope.launch {
            if (loginRepository.listUser().isEmpty()) {
                loginRepository.fillUser(User("admin", "Victor", "Bravo", "admin", 1))
                listMusicAnime.map {
                    loginRepository.fillListAnime(it)
                }
            }
        }
    }

    fun validate(user: String, password: String) {
        viewModelScope.launch {
            val response = when {
                user.length < 4 && password.length < 4 -> RespValidate.Error(message = "Username and password must be at least 4 characters")
                user.length < 4 -> RespValidate.Error(message = "Username must be at least 4 characters")
                password.length < 4 -> RespValidate.Error(message = "Password must be at least 4 characters")
                else -> {
                    val response = loginRepository.listUser()
                        .filter { it.user == user && it.password == password }
                    if (response.isNotEmpty()) {
                        loginRepository.createUser(response[0].copy(session = true))
                        RespValidate.Success
                    } else RespValidate.Error(message = "Invalid credentials")
                }
            }
            _validationState.value = response
        }
    }


}