package com.example.animetunechallenge.navigation

sealed class Screen(val route:String){
    data object MainScreen: Screen("main_Screen")
    data object OptionScreen: Screen("option_Screen")
    data object LoginScreen: Screen("login_Screen")
    data object GameFree: Screen("game_free")
    data object GameTime: Screen("game_time")
    data object GameQuestion: Screen("game_question")
    data object Victory: Screen("victory")
    data object Defeat: Screen("defeat")
    data object Bugs: Screen("bugs")

    fun withArgs(vararg args:String):String {
        return buildString {
            append(route)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }
}
