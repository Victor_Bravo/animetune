package com.example.animetunechallenge.ui.theme.screen.optionScreen

import android.media.MediaPlayer
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavHostController
import com.example.animetunechallenge.R
import com.example.animetunechallenge.dataClass.ProgressNumber
import com.example.animetunechallenge.navigation.Screen
import androidx.lifecycle.compose.LocalLifecycleOwner


@Composable
fun OptionScreen(navController: NavHostController) {
    val context = LocalContext.current
    val mp: MediaPlayer = MediaPlayer.create(context, R.raw.techno_trance)
    mp.setVolume(0.7f, 0.7f)
    mp.isLooping = true
    val lifecycle = LocalLifecycleOwner.current.lifecycle
    ProgressNumber.progressNumber = 1f

    DisposableEffect(lifecycle) {
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_PAUSE) {
                mp.pause()
            } else if (event == Lifecycle.Event.ON_RESUME) {
                mp.start()
            }
        }
        lifecycle.addObserver(observer)
        onDispose {
            mp.stop()
            mp.release()
            lifecycle.removeObserver(observer)
        }
    }

    Box(
        modifier = Modifier
            .padding(top = 8.dp)
            .fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = "Modo de juego", modifier = Modifier
                .width(364.dp)
                .height(67.dp)
                .background(
                    brush = Brush.horizontalGradient(
                        colors = listOf(Color.Transparent, Color(0xFF272257)),
                        startX = 0f,
                        endX = 500f
                    )
                ),
            style = TextStyle(
                fontSize = 45.sp,
                fontWeight = FontWeight(400),
                color = Color(0xFFFFFFFF),
                textAlign = TextAlign.Center,
//                fontFamily = FontFamily(Font(R.font.alata))
            )
        )
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Bottom
    ) {

        Btn(image = R.drawable.dia, text = "Libre", 15) {
            navController.navigate(Screen.GameFree.route)
        }
        Btn(image = R.drawable.tarde, text = "Enigma", 15) {
            navController.navigate(Screen.GameQuestion.route)
        }
        Btn(image = R.drawable.noche, text = "Tiempo", 15) {
            navController.navigate(Screen.GameTime.route)
        }
        Box(
            modifier = Modifier.fillMaxWidth(),
            contentAlignment = Alignment.BottomEnd
        ) {
            Image(
                painter = painterResource(id = R.drawable.girl_menu_option2),
                contentDescription = "gird gamer option",
                modifier = Modifier
                    .width(240.dp)
                    .height(260.dp),
                contentScale = ContentScale.FillBounds
            )
        }
    }
}

@Composable
fun Btn(image: Int, text: String, margin: Int = 0, navigate: () -> Unit) {
    Box(
        modifier = Modifier
            .padding(margin.dp)
            .width(308.dp)
            .height(55.dp)
            .clip(shape = RoundedCornerShape(0.dp, 70.dp, 70.dp, 0.dp))
            .border(
                width = 4.dp,
                color = Color(0xFF5147A6),
                shape = RoundedCornerShape(0.dp, 70.dp, 70.dp, 0.dp)
            ),
        contentAlignment = Alignment.CenterStart
    ) {
        Image(
            painter = painterResource(id = image),
            contentDescription = "hogar anime",
            contentScale = ContentScale.Crop,
            colorFilter = ColorFilter.colorMatrix(colorMatrix = ColorMatrix()),
            modifier = Modifier
                .fillMaxSize()
                .clickable {
                    navigate()
                }
        )

        Text(
            text = text, style = TextStyle(
                fontSize = 20.sp,
//            fontFamily = FontFamily(Font(R.font.inter)),
                fontWeight = FontWeight(900),
                color = Color.White,
            ), modifier = Modifier
                .alpha(0.7f)
                .padding(start = 9.dp)
        )

        Text(
            text = text, style = TextStyle(
                fontSize = 20.sp,
//            fontFamily = FontFamily(Font(R.font.inter)),
//            fontWeight = FontWeight(600),
                color = Color(0xFF272257),
            ), modifier = Modifier
                .padding(start = 9.dp)
        )
    }

}