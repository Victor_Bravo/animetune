package com.example.animetunechallenge.ui.theme.screen.mainScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.animetunechallenge.R
import com.example.animetunechallenge.navigation.Screen

@Composable
fun MainScreen(navController: NavHostController, loginViewModel: MainScreenViewModel = hiltViewModel()) {
    var isSession by remember { mutableStateOf(false) }
    Box(
      modifier= Modifier.fillMaxSize(),
    ){
        Image(painter = painterResource(R.drawable.bg_main_screen), contentDescription = "hogar anime", contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize())
        loginViewModel.isSession { isSession = it }
        if (isSession){
            navController.navigate(Screen.OptionScreen.route) {
                popUpTo(Screen.MainScreen.route) { inclusive = true }
                launchSingleTop = true
            }
        } else {
            BtnForm(
                "Log in",
                modifier = Modifier.align(Alignment.BottomCenter).padding(10.dp),
                onClick = {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(Screen.MainScreen.route) { inclusive = true }
                    }
                })
            Spacer(modifier = Modifier.height(100.dp))
        }

    }
}

@Composable
fun BtnForm(text: String, onClick:(() -> Unit), modifier: Modifier = Modifier){
    Button(onClick = {
        onClick()
    }, modifier= modifier
        .fillMaxWidth()
        .height(60.dp),
        colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF5147A6)), ) {
        Text(text = text, fontSize = 20.sp )
    }
}