package com.example.animetunechallenge.util

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.animetunechallenge.R
import kotlinx.coroutines.flow.StateFlow

@Composable
fun Disco(modifier: Modifier = Modifier, isMusicCompletion: StateFlow<Boolean>, onClick: (Boolean) -> Unit={}) {
    var isRotating by remember { mutableStateOf(true) }
    var targetAngle by remember { mutableFloatStateOf(0f) }
    val isPlaying by isMusicCompletion.collectAsState()

    // Inicia la rotación automáticamente
    LaunchedEffect(Unit) {
        targetAngle = 1440f
    }

    // Calcula el ángulo de rotación
    val angle by animateFloatAsState(
        targetValue = targetAngle,
        animationSpec = tween(durationMillis = 18000, easing = LinearEasing),
        label = ""
    )
    if (isPlaying) {
        isRotating = false
        targetAngle = angle
    }


    Image(
        modifier = modifier
            .padding(16.dp)
            .size(200.dp)
            .rotate(angle) // Aplica la rotación aquí
            .pointerInput(Unit) {
                detectTapGestures {
                    onClick(isRotating)
                    isRotating = !isRotating
                    targetAngle = if (isRotating) angle + 1440f else angle
                }
            },
        painter = painterResource(R.drawable.disco),
        contentDescription = "Descripción de la imagen",
    )
}