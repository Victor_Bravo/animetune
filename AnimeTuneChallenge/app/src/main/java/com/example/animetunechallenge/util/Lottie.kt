package com.example.animetunechallenge.util

import android.media.MediaPlayer
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.example.animetunechallenge.R
import java.util.Timer
import java.util.TimerTask


@Composable
fun AnimatedPreloader(
    modifier: Modifier = Modifier,
    level: String = "1",
    onFinish: (isFinish: Boolean) -> Unit
) {
    val preloaderLottieComposition by rememberLottieComposition(
        LottieCompositionSpec.RawRes(
            R.raw.start
        )
    )
    val context = LocalContext.current

    val mp = remember { MediaPlayer.create(context, R.raw.song_level_up) }
    var isVisible by remember { mutableStateOf(false) }
    var isFinish by remember { mutableStateOf(false) }

    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (star, text, text2) = createRefs()
        LottieAnimation(
            composition = preloaderLottieComposition,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .height(300.dp)
                .constrainAs(star) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                },
        )

        Timer().schedule(object : TimerTask() {
            override fun run() {
                isVisible = true
            }
        }, 600)
        Timer().schedule(object : TimerTask() {
            override fun run() {
                isFinish = true
            }
        }, 900)

        if (isFinish) onFinish(true)

        AnimatedVisibility(isVisible, modifier = modifier.constrainAs(text) {
            start.linkTo(star.start)
            end.linkTo(star.end)
            top.linkTo(star.top)
            bottom.linkTo(star.bottom)
        }) {
            mp.start()
            Text(text = "${level.toInt() + 1}", fontSize = 60.sp)
        }
        AnimatedVisibility(!isVisible, modifier = modifier.constrainAs(text2) {
            start.linkTo(star.start)
            end.linkTo(star.end)
            top.linkTo(star.top)
            bottom.linkTo(star.bottom)
        }) {
            Text(text = level, fontSize = 60.sp)
        }
    }
}