package com.example.animetunechallenge.util

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.animetunechallenge.R
import com.example.animetunechallenge.ui.theme.Purple80

@Composable
fun TextFieldDecorate(
    placeholder: String,
    modifier: Modifier = Modifier,
    image: Int? = null,
    isPassword: Boolean = false,
    onValueChange: (String) -> Unit
) {
    var passwordVisibility by remember { mutableStateOf(!isPassword) }
    var textState by remember { mutableStateOf(value = "") }
    val visualTransformation = if (passwordVisibility) {
        VisualTransformation.None
    } else PasswordVisualTransformation()
    TextField(
        modifier = modifier.fillMaxWidth().border(
            width = 2.dp,
            brush = Brush.horizontalGradient(listOf(Purple80, Purple80)),
            shape = RoundedCornerShape(35.dp)
        ),
        visualTransformation = visualTransformation,
        textStyle = TextStyle(
            fontSize = 20.sp
        ),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
        ),
        trailingIcon = {},
        leadingIcon = {
            if (image != null) {
                val icon = when {
                    isPassword -> if (!passwordVisibility) painterResource(image) else painterResource(
                        R.drawable.lock_open
                    )

                    else -> painterResource(image)
                }
                IconButton(onClick = { if (isPassword) passwordVisibility = !passwordVisibility }) {
                    Icon(painter = icon, contentDescription = "Icono descriptivo")
                }
            }
        },

        value = textState,
        onValueChange = {
            textState = it
            onValueChange(it)
        },
        placeholder = {
            Text(
                text = placeholder,
                modifier = Modifier.fillMaxWidth(),
                style = TextStyle(
                    color = Color.LightGray,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                ),
                textAlign = TextAlign.Center
            )
        },

        colors = TextFieldDefaults.colors(
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
            cursorColor = Color.Blue,
            focusedContainerColor = Color.Transparent,
            unfocusedContainerColor = Color.Transparent,
            disabledContainerColor = Color.Transparent,
        )
    )
}