package com.example.animetunechallenge.dataClass

enum class Anime(val title: String) {
    DRAGON_BALL("Dragon Ball"),
    YUGIOH("Yu-Gi-Oh!"),
    NARUTO("Naruto"),
    TOKYO_GHOUL("Tokyo Ghoul"),
    JUJUTSU_KAISEN("Jujutsu Kaisen"),
    DEATH_NOTE("Death Note"),
    KIMETSU_NO_YAIBA("Kimetsu no Yaiba"),
    ATTACK_ON_TITAN("Attack on Titan"),
    ONE_PIECE("One Piece"),
    HAIKYUU("Haikyuu!!"),
    RE_ZERO("Re:ZERO -Starting Life in Another World-"),
    MY_HERO_ACADEMY("My Hero Academia"),
    TOKIO_REVENGERS("Tokyo Revengers"),
    KAKEGURUI("Kakegurui"),
    PRISSION_SCHOOL("Prison School"),
    YOUR_LIE_IN_APRIL("Your Lie in April"),
    DANGAROMPA("Danganronpa"),
    ASESINATION_CLASSROOM("Assassination Classroom")
}