package com.example.animetunechallenge.ui.theme.screen.mainScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.animetunechallenge.repository.LoginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModel @Inject constructor(
    private val loginRepository: LoginRepository
): ViewModel() {
    fun isSession(response: (Boolean)->Unit){
        viewModelScope.launch {
            val resp = loginRepository.listUser().filter { it.session }
            response(resp.isNotEmpty())
        }
    }
}