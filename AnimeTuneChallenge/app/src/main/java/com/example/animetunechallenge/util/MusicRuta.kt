package com.example.animetunechallenge.util

import com.example.animetunechallenge.R
import com.example.animetunechallenge.dataClass.Anime

fun musicRuta(keys: String): Int {
    return when (keys) {
        Anime.YUGIOH.name -> R.raw.yugiho
        Anime.DRAGON_BALL.name -> R.raw.dragon_ball_z_chala_head
        Anime.NARUTO.name -> R.raw.naruto_opening
        Anime.TOKYO_GHOUL.name -> R.raw.tokyo_ghoul_opening
        Anime.ATTACK_ON_TITAN.name -> R.raw.attack_on_titan
        Anime.DEATH_NOTE.name -> R.raw.death_note
        else -> R.raw.tokyo_ghoul_opening
    }
}